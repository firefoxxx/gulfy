#Birthday Special from JISAN
"""
happy = "╭╮╱╭╮\n┃┃╱┃┃\n┃╰━╯┣━━┳━━┳━━┳╮╱╭╮\n┃╭━╮┃╭╮┃╭╮┃╭╮┃┃╱┃┃\n┃┃╱┃┃╭╮┃╰╯┃╰╯┃╰━╯┃\n╰╯╱╰┻╯╰┫╭━┫╭━┻━╮╭╯\n╱╱╱╱╱╱╱┃┃╱┃┃╱╭━╯┃\n╱╱╱╱╱╱╱╰╯╱╰╯╱╰━━╯\n\n"

birth = "╭━━╮╱╱╱╭╮╭╮\n┃╭╮┃╱╱╭╯╰┫┃\n┃╰╯╰┳┳┻╮╭┫╰━╮\n┃╭━╮┣┫╭┫┃┃╭╮┃\n┃╰━╯┃┃┃┃╰┫┃┃┃\n╰━━━┻┻╯╰━┻╯╰╯\n\n"

day = "╭━━━╮\n╰╮╭╮┃\n╱┃┃┃┣━━┳╮╱╭╮\n╱┃┃┃┃╭╮┃┃╱┃┃\n╭╯╰╯┃╭╮┃╰━╯┃\n╰━━━┻╯╰┻━╮╭╯\n╱╱╱╱╱╱╱╭━╯┃\n╱╱╱╱╱╱╱╰━━╯\n\n"

goodcat = "╭━━━╮╱╱╱╱╱╱╱╭┳━━━╮╱╱╭╮\n┃╭━╮┃╱╱╱╱╱╱╱┃┃╭━╮┃╱╭╯╰╮\n┃┃╱╰╋━━┳━━┳━╯┃┃╱╰╋━┻╮╭╯\n┃┃╭━┫╭╮┃╭╮┃╭╮┃┃╱╭┫╭╮┃┃\n┃╰┻━┃╰╯┃╰╯┃╰╯┃╰━╯┃╭╮┃╰╮\n╰━━━┻━━┻━━┻━━┻━━━┻╯╰┻━╯\n\n"

sandy = "╭━━━╮╱╱╱╱╱╱╱╭╮\n┃╭━╮┃╱╱╱╱╱╱╱┃┃\n┃╰━━┳━━┳━╮╭━╯┣╮╱╭╮\n╰━━╮┃╭╮┃╭╮┫╭╮┃┃╱┃┃\n┃╰━╯┃╭╮┃┃┃┃╰╯┃╰━╯┃\n╰━━━┻╯╰┻╯╰┻━━┻━╮╭╯\n╱╱╱╱╱╱╱╱╱╱╱╱╱╭━╯┃\n╱╱╱╱╱╱╱╱╱╱╱╱╱╰━━╯\n\n"

to you = "╱╭╮╱╱╭╮╱╱╭╮\n╭╯╰╮╱┃╰╮╭╯┃\n╰╮╭╋━┻╮╰╯╭┻━┳╮╭╮\n╱┃┃┃╭╮┣╮╭┫╭╮┃┃┃┃\n╱┃╰┫╰╯┃┃┃┃╰╯┃╰╯┃\n╱╰━┻━━╯╰╯╰━━┻━━╯\n\n"
"""

import asyncio
from ..utils import admin_cmd

@borg.on(admin_cmd(pattern=f"hbd$", outgoing=True))
async def _(event):
    if event.fwd_from:
        return
    animation_interval = 1
    animation_ttl = range(0, 4)
    await event.edit(
        "╭━━━╮╱╱╱╱╱╱╱╭╮\n┃╭━╮┃╱╱╱╱╱╱╱┃┃\n┃╰━━┳━━┳━╮╭━╯┣╮╱╭╮\n╰━━╮┃╭╮┃╭╮┫╭╮┃┃╱┃┃\n┃╰━╯┃╭╮┃┃┃┃╰╯┃╰━╯┃\n╰━━━┻╯╰┻╯╰┻━━┻━╮╭╯\n╱╱╱╱╱╱╱╱╱╱╱╱╱╭━╯┃\n╱╱╱╱╱╱╱╱╱╱╱╱╱╰━━╯"
    )
    animation_chars = [
        "╭╮╱╭╮\n┃┃╱┃┃\n┃╰━╯┣━━┳━━┳━━┳╮╱╭╮\n┃╭━╮┃╭╮┃╭╮┃╭╮┃┃╱┃┃\n┃┃╱┃┃╭╮┃╰╯┃╰╯┃╰━╯┃\n╰╯╱╰┻╯╰┫╭━┫╭━┻━╮╭╯\n╱╱╱╱╱╱╱┃┃╱┃┃╱╭━╯┃\n╱╱╱╱╱╱╱╰╯╱╰╯╱╰━━╯",
        "╭━━╮╱╱╱╭╮╭╮\n┃╭╮┃╱╱╭╯╰┫┃\n┃╰╯╰┳┳┻╮╭┫╰━╮\n┃╭━╮┣┫╭┫┃┃╭╮┃\n┃╰━╯┃┃┃┃╰┫┃┃┃\n╰━━━┻┻╯╰━┻╯╰╯",
        "╭━━━╮\n╰╮╭╮┃\n╱┃┃┃┣━━┳╮╱╭╮\n╱┃┃┃┃╭╮┃┃╱┃┃\n╭╯╰╯┃╭╮┃╰━╯┃\n╰━━━┻╯╰┻━╮╭╯\n╱╱╱╱╱╱╱╭━╯┃\n╱╱╱╱╱╱╱╰━━╯",
        "╱╭╮╱╱╭╮╱╱╭╮\n╭╯╰╮╱┃╰╮╭╯┃\n╰╮╭╋━┻╮╰╯╭┻━┳╮╭╮\n╱┃┃┃╭╮┣╮╭┫╭╮┃┃┃┃\n╱┃╰┫╰╯┃┃┃┃╰╯┃╰╯┃\n╱╰━┻━━╯╰╯╰━━┻━━╯",
    ]
    for i in animation_ttl:
        await asyncio.sleep(animation_interval)
        await event.edit(animation_chars[i % 4])
    await asyncio.sleep(animation_interval)
    await event.edit(
        "╭╮╱╭╮\n┃┃╱┃┃\n┃╰━╯┣━━┳━━┳━━┳╮╱╭╮\n┃╭━╮┃╭╮┃╭╮┃╭╮┃┃╱┃┃\n┃┃╱┃┃╭╮┃╰╯┃╰╯┃╰━╯┃\n╰╯╱╰┻╯╰┫╭━┫╭━┻━╮╭╯\n╱╱╱╱╱╱╱┃┃╱┃┃╱╭━╯┃\n╱╱╱╱╱╱╱╰╯╱╰╯╱╰━━╯\n\n╭━━╮╱╱╱╭╮╭╮\n┃╭╮┃╱╱╭╯╰┫┃\n┃╰╯╰┳┳┻╮╭┫╰━╮\n┃╭━╮┣┫╭┫┃┃╭╮┃\n┃╰━╯┃┃┃┃╰┫┃┃┃\n╰━━━┻┻╯╰━┻╯╰╯\n\n╭━━━╮\n╰╮╭╮┃\n╱┃┃┃┣━━┳╮╱╭╮\n╱┃┃┃┃╭╮┃┃╱┃┃\n╭╯╰╯┃╭╮┃╰━╯┃\n╰━━━┻╯╰┻━╮╭╯\n╱╱╱╱╱╱╱╭━╯┃\n╱╱╱╱╱╱╱╰━━╯\n\n╭━━━╮╱╱╱╱╱╱╱╭┳━━━╮╱╱╭╮\n┃╭━╮┃╱╱╱╱╱╱╱┃┃╭━╮┃╱╭╯╰╮\n┃┃╱╰╋━━┳━━┳━╯┃┃╱╰╋━┻╮╭╯\n┃┃╭━┫╭╮┃╭╮┃╭╮┃┃╱╭┫╭╮┃┃\n┃╰┻━┃╰╯┃╰╯┃╰╯┃╰━╯┃╭╮┃╰╮\n╰━━━┻━━┻━━┻━━┻━━━┻╯╰┻━╯"
    )